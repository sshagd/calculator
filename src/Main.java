import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    static Map<Integer, String> numbers = new LinkedHashMap<>();
    static {
        numbers.put(1, "I");
        numbers.put(2, "II");
        numbers.put(3, "III");
        numbers.put(4, "IV");
        numbers.put(5, "V");
        numbers.put(6, "VI");
        numbers.put(7, "VII");
        numbers.put(8, "VIII");
        numbers.put(9, "IX");
        numbers.put(10, "X");
        numbers.put(30, "XXX");
        numbers.put(40, "XL");
        numbers.put(20, "XX");
        numbers.put(50, "L");
        numbers.put(60, "LX");
        numbers.put(70, "LXX");
        numbers.put(80, "LXXX");
        numbers.put(90, "XC");
        numbers.put(100, "C");
    }

    public static void main(String[] args) {
        System.out.println("Калькулятор работает с двумя римскими или арабскими числами от 1 до 10 включительно, умеет складывать, " +
                "вычитать, умножать и делить.\nВведите условие (например: I * III):");
        Scanner in = new Scanner(System.in);

        if(in.hasNextInt()) {
            int result = new Converter().convertArab(in);
            System.out.println("Ответ:\n" + result);
        } else if(in.hasNext()){
            String result = new Converter().convertRim(in);
            if(result != null){
                System.out.println("Ответ:\n" + result);
            }
        } else {
            System.out.println("WRONG STRING");
        }
    }
}
