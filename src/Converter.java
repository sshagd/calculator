import java.util.Scanner;

public class Converter {

    public int convertArab(Scanner in){
        int a = in.nextInt();
        String operation = in.next();
        int b = in.nextInt();
        int result = selection(a, b, operation);

        if (!isValid(a) || !isValid(b)) {
            System.out.println("WRONG NUMBER");
            result = 0;
        }

        return result;
    }

    public String convertRim(Scanner in){
        String rimResult = "";
        String operation;
        String rimB = "";
        String rimA = in.next();
        if(rimToArab(rimA) > 0) {
            operation = in.next();
            rimB = in.next();
            int result = computing(rimA, rimB, operation);
            if (result <= 10) {
                rimResult = Main.numbers.get(result);
            } else if (result % 10 > 0) {
                int tens = result / 10;
                int units = result % 10;
                rimResult = Main.numbers.get(tens * 10) + Main.numbers.get(units);
            } else {
                int tens = result / 10;
                rimResult = Main.numbers.get(tens * 10);
            }
        } else {
            System.out.println("Условие введено неверно");
            return null;
        }
        return rimResult;
    }

    private int computing(String rimA, String rimB, String operation){
        int result = 0;
        int a = rimToArab(rimA);
        int b = rimToArab(rimB);
        if((a > 0) && (b > 0)) {
            result = selection(a, b, operation);
        } else {
            System.out.println("WRONG NUMBER");
        }
        return result;
    }

    private int rimToArab(String rimNumber){
        int arabNumber = 0;
        for(int i = 1; i <= 10; i ++) {
            if (Main.numbers.get(i).equals(rimNumber)) {
                arabNumber = i;
            }
        }
        return arabNumber;
    }

    private int selection(int a, int b, String operation){
        int result = 0;
        ArabNumbers arab = new ArabNumbers();

        switch (operation) {
            case "+":
                result = arab.addition(a, b);
                break;
            case "-":
                result = arab.subtraction(a, b);
                break;
            case "*":
                result = arab.multiplication(a, b);
                break;
            case "/":
                result = arab.division(a, b);
                break;
            default:
                System.out.println("Некорректная операция");
                break;
        }
        return result;
    }

    private boolean isValid(int number){
        return (number > 0) && (number <= 10);
    }
}
